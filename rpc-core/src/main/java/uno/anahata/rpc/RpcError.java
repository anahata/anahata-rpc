package uno.anahata.rpc;

/*-
 * #%L
 * rpc-core
 * %%
 * Copyright (C) 2020 <A HREF="http://www.anahata.uno">Anahata</A>
 * %%
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * #L%
 */

import java.io.Serializable;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.SerializationUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;

/**
 * Represents a server side error on a RPC call, a serialized exception of the 
 * original exception and a string representation of the same are kept so if
 * the caller side cannot deserialize it (e.g. exception not in client class path)
 * a string representation of the server side exception can be given to the caller.
 *
 * @author priyadarshi 
 */
@NoArgsConstructor
@Slf4j
public class RpcError implements Serializable {
    
    /**
     * The serialized exception.
     */
    private byte[] exception;

    /**
     * The exception stack trace in string format.
     */
    private String exceptionString;

    /**
     * Constructor giving the server side exception.
     *
     * @param t
     */
    public RpcError(Throwable t) {
        try {
            exception = SerializationUtils.serialize(t);
        } catch (Exception e) {
            log.debug("Non serializable exception", e);
        }
        
        exceptionString = ExceptionUtils.getStackTrace(t);
    }

    /**
     * Gets the server side exception if it can be deserialized or a {@link NonDeserializableException} with a string
     * version of the server side exception stack trace if it can't.
     *
     * @return
     */
    public Throwable getException() {
        try {
            return (Throwable)SerializationUtils.deserialize(exception);
        } catch (Exception e) {
            return new NonDeserializableException(exceptionString);
        }
    }

    /**
     * the server side exception if it can be deserialized or a {@link NonDeserializableException} with a string version
     * of the server side exception stack trace if it can't.
     *
     * @throws Throwable
     */
    public void doThrow() throws Throwable {
        Throwable t = getException();
        log.debug("RpcError throwing {}", t.toString());
        throw t;
    }

    @Override
    public String toString() {
        return "RcpError{" + "exception=" + exception + ", exceptionString=" + exceptionString + '}';
    }
}
