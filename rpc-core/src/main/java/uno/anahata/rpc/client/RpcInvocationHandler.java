package uno.anahata.rpc.client;

/*-
 * #%L
 * rpc-core
 * %%
 * Copyright (C) 2020 <A HREF="http://www.anahata.uno">Anahata</A>
 * %%
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * #L%
 */
import uno.anahata.rpc.Rpc;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import uno.anahata.rpc.RpcResponse;

/**
 * JDK proxy InvocationHandler for RPC calls. Intercepts the call on the proxy
 * stub and delegates the invocation to the {@link #transport}
 *
 * @author priyadarshi 
 */
@AllArgsConstructor
@Slf4j
public class RpcInvocationHandler implements InvocationHandler {

    //private static Logger log = LoggerFactory.getLogger(RpcInvocationHandler.class);
    /**
     * The Interface used for the stub
     */
    @Getter
    private Class stubClass;

    /**
     * The transport for the rpc call
     */
    @Getter
    private RpcClientTransport transport;

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        log.debug("RpcInvocationHandler entry {} {}.{} ", method.getReturnType(),
                stubClass.getSimpleName(),
                method.getName());

        if (method.getDeclaringClass().equals(Object.class)) {
            return method.invoke(this, args);
        }

        Rpc req = new Rpc(stubClass, method, args);

        long ts = System.currentTimeMillis();
        RpcResponse resp = transport.sendReceive(req);
        ts = System.currentTimeMillis() - ts;

        log.debug("{} ms full round trip for {} ", ts, method);

        if (resp.getError() != null) {
            throw resp.getError().getException().fillInStackTrace();
            //resp.getError().doThrow();
        }

        return resp.getResponse();
    }

}
