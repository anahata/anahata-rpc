package uno.anahata.rpc;

/*-
 * #%L
 * rpc-core
 * %%
 * Copyright (C) 2020 <A HREF="http://www.anahata.uno">Anahata</A>
 * %%
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * #L%
 */

import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.Validate;
import org.apache.commons.lang3.reflect.MethodUtils;

/**
 * Serialization friendly representation of method invocation (method signature and arguments) 
 * that can actually be invoked on an instance {@link #getResponse(java.lang.Object)}.
 * <br>
 * 
 * Instances of this type are typically serialized on the caller side, sent over
 * a network to a different JVM, deserialized and invoked on a target object.
 * 
 * <br>
 * 
 * Some example applications:
 
 * <ul>
 *    <li>A Desktop client invoking methods on EJBs on an application server</li>
 *    <li>A Desktop client receiving push notifications from an application server over JMS</li>
 *    <li>A Peer receiving invocations from another peer on a p2p application</li>
 * </ul>
 * 
 * 
 * @author priyadarshi 
 */
@Getter
@Slf4j
public final class Rpc implements Serializable {

    /**
     * Contains a string representation of primitive types (and primitive type arrays).
     */
    private static final Map<String, Class<?>> primitiveClasses = new HashMap<>();

    static {
        primitiveClasses.put("byte", byte.class);
        primitiveClasses.put("byte[]", byte[].class);
        primitiveClasses.put("short", short.class);
        primitiveClasses.put("short[]", short[].class);
        primitiveClasses.put("char", char.class);
        primitiveClasses.put("char[]", char[].class);
        primitiveClasses.put("int", int.class);
        primitiveClasses.put("int[]", int[].class);
        primitiveClasses.put("long", long.class);
        primitiveClasses.put("long[]", long[].class);
        primitiveClasses.put("float", float.class);
        primitiveClasses.put("float[]", float[].class);
        primitiveClasses.put("double", double.class);
        primitiveClasses.put("double[]", double[].class);
        primitiveClasses.put("boolean", boolean.class);
        primitiveClasses.put("boolean[]", boolean[].class);
    }

    /**
     * The name of the class of the object to be invoked.
     */
    private final String clazzName;

    /**
     * The name of the method in the class of the object to be invoked.
     */
    private final String methodName;

    /**
     * The name of the types of the parameters of the method to be invoked.
     */
    private final String[] methodParamTypeNames;

    /**
     * The arguments of the method to be invoked.
     */
    private final Object[] args;

    /**
     * Cache of the resolved class to be invoked (non serializable)
     */
    private transient Class clazz = null;

    /**
     * Cache of the method to be invoked (non serializable)
     */
    private transient Method method = null;

    /**
     * Cache of the parameters of the method to be invoked.
     */
    private transient Class[] methodParamTypes = null;

    /**
     * Cache of the arguments for the method to be invoked after the types have been checked for serialization type mismatches (like hessian when serializing a char[] as a String).
     */
    private transient Object[] checkedArgs = null;

    public Rpc(Method method, Object[] args) {
        this(method.getDeclaringClass(), method, args);
    }

    public Rpc(Class clazz, Method method, Object[] args) {
        this.clazz = clazz;
        this.clazzName = clazz.getName();
        this.method = method;
        this.methodName = method.getName();
        this.methodParamTypes = method.getParameterTypes();
        this.methodParamTypeNames = new String[method.getParameterTypes().length];
        for (int i = 0; i < methodParamTypeNames.length; i++) {
            methodParamTypeNames[i] = className(method.getParameterTypes()[i]);
        }
        this.args = args;
        this.checkedArgs = args;
    }

    /**
     * Gets the target class resolving it the first time this method is invoked after deserialization.
     *
     * @return The resolved class (after serialization)
     * @throws RuntimeException if the class cannot be loaded
     */
    public Class getClazz() {
        if (clazz != null) {
            return clazz;
        }
        try {
            clazz = Class.forName(clazzName);
        } catch (ClassNotFoundException e) {
            throw new RuntimeException("Could not load class " + clazzName, e);
        }
        
        return clazz;
    }

    /**
     * Gets the target method resolving it the first time this method is invoked after deserialization.
     *
     * @return
     * @throws NoSuchMethodException
     * @throws ClassNotFoundException
     */
    public Method getMethod() throws NoSuchMethodException, ClassNotFoundException {
        if (method != null) {
            return method;
        }
        Class loadedClazz = getClazz();
        log.trace("Class {}, ", loadedClazz);
        log.trace("Parameter Types {}, ", (Object[])getMethodParamTypes());
        log.trace("Method name {}, ", methodName);
        method = MethodUtils.getAccessibleMethod(loadedClazz, methodName, getMethodParamTypes());
        Validate.notNull(method, "Could not find accessible method for %s.%s %s", getClazz(), methodName,
                getMethodParamTypes());
        log.trace("Method {}, ", method);
        return method;
    }

    /**
     * Gets the method parameter types.
     *
     * @return
     * @throws ClassNotFoundException
     */
    private Class[] getMethodParamTypes() {
        if (methodParamTypes != null) {
            return this.methodParamTypes;
        }
        Class[] ret = new Class[methodParamTypeNames.length];
        for (int i = 0; i < methodParamTypeNames.length; i++) {
            try {
                ret[i] = loadClass(methodParamTypeNames[i]);
            } catch (ClassNotFoundException e) {
                throw new RuntimeException(e);
            }

        }
        this.methodParamTypes = ret;
        return ret;
    }

    /**
     * Gets the method arguments, checking performing type conversion if required.
     *
     * @return the method arguments.
     */
    public Object[] getArgs() {        
        return args;
    }
    
    /**
     * Invokes the rpc on the given object.
     *
     * @param target the target object
     * @return whatever the object returns
     *
     * @throws NoSuchMethodException
     * @throws ClassNotFoundException
     * @throws IllegalAccessException
     * @throws IllegalArgumentException
     * @throws InvocationTargetException
     */
    public Object invoke(Object target) throws NoSuchMethodException, ClassNotFoundException, IllegalAccessException,
            IllegalArgumentException, InvocationTargetException {
        log.debug("Invoking {} on {} with args {}", getMethod(), target, getArgs());
        Object ret = getMethod().invoke(target, getArgs());
        log.debug("INVOKED {} on {} with args {} returned {}", getMethod(), target, getArgs(), ret);
        return ret;
    }
    
    /**
     * Performes the method invocation represented by this object on a target 
     * and wraps the returned value or thrown exception onto a {@link RpcResponse}.
     * 
     * @param target - the target object (which should implement {@link #getMethod()}
     * @return the response containing either the returned object or the exception thrown.
     */
    public RpcResponse getResponse(Object target) {
        try {
            return new RpcResponse(invoke(target));
        } catch (Exception e) {
            log.error("Exception invoking {} on {}", this, target, e);
            return new RpcResponse(new RpcError(e));
        }
    }

    @Override
    public String toString() {
        return "RpcRequest{" + "clazzName=" + clazzName + ", methodName=" + methodName + ", methodParamTypeNames=" + Arrays.toString(
                methodParamTypeNames) + ", args=" + Arrays.toString(args) + '}';
    }

    private static Class<?> loadClass(String name) throws ClassNotFoundException {
        if (primitiveClasses.containsKey(name)) {
            return primitiveClasses.get(name);
        } else {
            return Class.forName(name);
        }
    }

    private static String className(Class clazz) {
        Set<Map.Entry<String, Class<?>>> set = primitiveClasses.entrySet();
        for (Entry<String, Class<?>> entry : set) {
            if (entry.getValue() == clazz) {
                return entry.getKey();
            }
        }
        return clazz.getName();
    }


}
